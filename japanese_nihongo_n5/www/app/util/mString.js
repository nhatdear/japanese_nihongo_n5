/**
 * Created by nhatdear on 3/16/2016.
 */
define([], function() {
    'use strict';
    var mString = {
        MINNA1 : "Minna no Nihongo 1",
        WELCOME : "Giới thiệu",
        UNITS : "Bài học Minna 1-25",
        UNIT  : "Bài"
    };

    return mString;
})