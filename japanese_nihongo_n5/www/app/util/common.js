/**
 * Created by nhatdear on 3/16/2016.
 */
define([], function() {
    var Common = {
        VIEWS : {
            WELCOME_VIEW: "viewmodels/welcome",
            UNIT_VIEW: "viewmodels/units",
        },
    }
    return Common;
})