/**
 * Created by nhatdear on 3/16/2016.
 */
define(['database/database'],function(DB) {
    var ctor = function () {
        this.features = [];
        var dataList = DB.getUnitsForN5();
        for (var i = 0; i < dataList.length ; i++){
            this.features[i] = {
                id : i,
                displayName : dataList[i]
            }
        }
        this.onUnitClick = function(unit){
            alert("Click on unit : " + unit.displayName + " with id : " + unit.id);
        };
    };
    return ctor;
});