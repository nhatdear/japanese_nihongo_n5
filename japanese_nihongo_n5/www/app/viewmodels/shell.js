﻿define(['plugins/router', 'durandal/app','util/mString','util/common'], function (router, app, mString,Common) {
    return {
        router: router,
        search: function() {
            //It's really easy to show a message box.
            //You can add custom options too. Also, it returns a promise for the user's response.
            app.showMessage('Search not yet implemented...');
        },
        activate: function () {
            router.map([
                { route: '', title:mString.WELCOME, moduleId: Common.VIEWS.WELCOME_VIEW, nav: true },
                { route: 'units', title:mString.UNITS, moduleId: Common.VIEWS.UNIT_VIEW, nav: true },
            ]).buildNavigationModel();

            router.mString = mString;
            return router.activate();
        }
    };
});